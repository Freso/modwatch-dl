#!/usr/bin/env python3
#
# Modwat.ch Download Links
# Copyright © 2017 Frederik “Freso” S. Olesen <https://freso.dk/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
Script to get download links for modwat.ch listed plugins

Usage:
  modwatch-dl.py <username> <game>

<username>: If your modlist is available at https://modwat.ch/u/Peanut, then
            "Peanut" is your username.
<game>: One of "skyrim", "skyrimse", or "fallout4".
"""

import yaml
import sys
from urllib.request import urlopen


def loot_plugin_url_map(game):
    """Return a dictionary mapping plugins to URLs."""
    loot_masterlist = "https://github.com/loot/{}/raw/v0.10/masterlist.yaml"
    masterlist = yaml.load(urlopen(loot_masterlist.format(game)))
    plugin_url_mapping = {}
    for plugin in masterlist['plugins']:
        if 'url' in plugin:
            plugin_url_mapping[plugin['name']] = plugin['url']
    return plugin_url_mapping


def loadorder_to_links(username, game):
    """Return a text mapping of plugins and download links."""
    mw_plugins = "https://modwatchapi-ansballard.rhcloud.com/api/user/{}/file/plugins"
    loadorder = yaml.load(urlopen(mw_plugins.format(username)))
    masterlist = loot_plugin_url_map(game)
    output = []
    for plugin in loadorder:
        if plugin in masterlist:
            plugin_url = masterlist[plugin]
            if len(plugin_url) == 1:
                plugin_url = plugin_url[0]
            output.append("{} ( {} )".format(plugin, plugin_url))
        else:
            output.append(plugin)
    return '\n'.join(output)


def main(args):
    """Handles base logic flow of the program."""
    if len(args) > 2:
        print("Too many arguments!")
        exit(1)
    elif len(args) < 2:
        print("Too few arguments!")
        exit(1)

    username = args[0]
    game = args[1]
    print(loadorder_to_links(username, game))


if __name__ == "__main__":
    main(sys.argv[1:])
